# Forge Atlassian Events for Slack

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE)

This is an example [Forge](https://developer.atlassian.com/platform/forge/) app that emits Slack messages when certain Atlassian product events (e.g. "issue created" or "page commented") occur.

![Screenshot of Slack messages](./screenshot.png)

## Usage

By default, Slack messages are sent for _every_ supported Forge [product event](https://developer.atlassian.com/platform/forge/product-events-reference/) that occurs in a Jira or Confluence sites that the app is installed into.

This app is designed as a reference example for developers. There are several opportunities to customize the app's behaviour by modifying the code, for example:

- Restricting which types of events are emitted by removing the corresponding event strings from [`manifest.yml`](./manifest.yml).

- Implementing your own event filtering logic by modifying [`jira-trigger.js`](./jira-trigger.js) or [`confluence-trigger.js`](./confluence-trigger.js).

- Changing the format of the messages emitted to Slack by modifying the appropriate [`formatter`](./formatter/). 

## Installation

**Note:** You will need permission to create and install a Slack app into your team's Slack workspace. Alternatively, create a new Slack workspace for testing purposes at [slack.com](https://slack.com).

1. Follow the instructions on [Example Apps](https://developer.atlassian.com/platform/forge/example-apps/) to copy, deploy, and install this Forge app.
1. Create a new Slack app at [api.slack.com](https://api.slack.com) (you can name it _Forge Slack Events_, or whatever you like).
1. In the app's configuration, click on **OAuth & Permissions**.
1. Scroll down to **Bot Token Scopes** and add the `chat:write` scope.
1. Click **Install app into Workspace** at the top of the page and install the app into your workspace.
1. Set an encrypted [environment variable](https://developer.atlassian.com/platform/forge/environments/) keyed by `SLACK_OAUTH_BEARER_TOKEN` with a value of the **Bot User OAuth Access Token**. `forge variables:set --encrypt SLACK_OAUTH_BEARER_TOKEN xxxxxxxxxx`.
1. Set the value of the `TARGET_SLACK_CHANNEL` environment variable to `#general` (or another channel of your choosing). This is the channel where messages will be sent. There is no need to encrypt this variable. `forge variables:set TARGET_SLACK_CHANNEL #general`.
1. Invite the bot to the channel you chose in the step above by typing `/invite @<botname>` in the appropriate Slack channel.
1. Run the `forge deploy` CLI command to deploy the changes to your environment variables.
1. You're done! Test out the app by creating a new issue in Jira, or commenting on a page in Confluence.

## Debugging

You can enable verbose logging by setting the `DEBUG_LOGGING` [environment variable](https://developer.atlassian.com/platform/forge/environments/) to `1`. Logs can then be viewed with the `forge logs` command.

Alternatively, you can use the [`forge tunnel`](https://developer.atlassian.com/platform/forge/change-the-frontend-with-forge-ui/#set-up-tunneling) command to run your Forge app locally. Note that you must pass the environment variable values to the tunnel with the prefix `FORGE_USER_VAR_`, e.g.:

```
FORGE_USER_VAR_TARGET_SLACK_CHANNEL=#general FORGE_USER_VAR_SLACK_OAUTH_BEARER_TOKEN=your_secret_bot_token_here FORGE_USER_VAR_DEBUG_LOGGING=1 forge tunnel
```

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![With â¤ï¸ from Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers.png)](https://www.atlassian.com)