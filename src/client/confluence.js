import { error, warn, debug } from '../util/logger';
import { fetchUser as fetchJiraUser } from './jira'

export async function fetchContent(contentId, fields) {
  let expandQuery = '';
  if (fields && fields.length > 0) {
    expandQuery = `?expand=${fields.join(',')}`;
  }
  debug(`/rest/api/content/${contentId}${expandQuery}`);
  return doGet(`/rest/api/content/${contentId}${expandQuery}`);
}

export async function fetchUser(accountId) {
  try {
    return await doGet(`/rest/api/user?accountId=${accountId}`);
  } catch (e) {
    return workaround_XIBF_82(accountId);
  }
}

// TODO remove once XIBF-82 is resolved
async function workaround_XIBF_82(accountId) {
  try {
    warn(`Fetching Confluence user ${accountId} from Jira to workaround XIBF-82`);
    return await fetchJiraUser(accountId);
  } catch (e) {
    warn(`Couldn't resolve user ${accountId} from Confluence or Jira`);
    return {
      displayName: "Unknown user"
    }
  }
}

async function doGet(restUrl) {
  const response = await api.asApp().requestConfluence(restUrl);
  if (!response.ok) {
    const err = `Error invoking ${restUrl} (Confluence): ${response.status} ${response.statusText}`;
    error(err);
    throw new Error(err);
  }
  const responseBody = await response.json();
  debug(`Response from Confluence: ${JSON.stringify(responseBody)}`);
  return responseBody;
}