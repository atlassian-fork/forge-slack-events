import { fetchUser } from './client/jira';
import { apiRequest as slackRequest } from './client/slack'
import { targetSlackChannel } from './config'
import { debug, warn } from './util/logger';
import { getEventFormatter } from './formatter/jira';

export async function handleJiraEvent(event) {
  debug(`Event ${JSON.stringify(event)}`);
  const { accountId, activityItem } = event;
  const { eventType, object } = activityItem;

  // Fetch the user who initiated the event
  const actor = await fetchUser(accountId);
  debug(`Actor ${JSON.stringify(actor)}`);

  // Format the event as a Slack message
  const formatter = getEventFormatter(object.type, eventType);
  if (!formatter) {
    warn(`No Jira formatter found for ${object.type}:${eventType}`);
    return;
  }
  const requestBody = await formatter(activityItem, actor);
  requestBody.channel = targetSlackChannel;

  // Dispatch the message to Slack 
  await slackRequest('chat.postMessage', requestBody);  
}
