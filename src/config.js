const { 
  TARGET_SLACK_CHANNEL,
  SLACK_OAUTH_BEARER_TOKEN,
  DEBUG_LOGGING
} = process.env;

const errors = [];

if (!TARGET_SLACK_CHANNEL) {
  errors.push('TARGET_SLACK_CHANNEL must be set to the target channel name or id');
}

if (!SLACK_OAUTH_BEARER_TOKEN) {
  errors.push('SLACK_OAUTH_BEARER_TOKEN must be set to the OAuth bearer token for your Slack app')
}

if (errors.length > 0) {
  console.error(
    `${errors.length} environment variables are missing or misconfigured:` + 
    `${errors.join('\n ')}` +
    `Please see README.md for further details.`
  );
  throw new Error('Environment variable config error');
}

export const targetSlackChannel = TARGET_SLACK_CHANNEL;
export const slackOAuthBearerToken = SLACK_OAUTH_BEARER_TOKEN;
export const debugLogging = DEBUG_LOGGING && true;
