import { fetchContent } from '../client/confluence';
import { debug, error } from '../util/logger';
import { findContainer, formatUserLink, html2mrkdwn } from './common';

const contentEventFormatters = {  
  "PUBLISHED": async (activityItem, actor) => {
    const { object } = activityItem;
    return {
      text: `*${formatUserLink(activityItem, actor)}* published *${object.name}* in *${formatSpaceLink(activityItem)}*`,
      attachments: [await formatContentAttachment(activityItem)]
    };
  },
  "VIEWED": async (activityItem, actor) => {
    const { object } = activityItem;
    return {
      text: `*${formatUserLink(activityItem, actor)}* viewed *${object.name}* in *${formatSpaceLink(activityItem)}*`,
      attachments: [await formatContentAttachment(activityItem)]
    };
  },
  "LIKED": async (activityItem, actor) => {
    const { object } = activityItem;
    return {
      text: `*${formatUserLink(activityItem, actor)}* liked *${object.name}* in *${formatSpaceLink(activityItem)}*`,
      attachments: [await formatContentAttachment(activityItem)]
    };
  },
  "COMMENTED": async (activityItem, actor) => {
    const { object } = activityItem;
    return {
      text: `*${formatUserLink(activityItem, actor)}* commented on *${object.name}* in *${formatSpaceLink(activityItem)}*`,
      attachments: [await formatCommentAttachment(activityItem)]
    };
  }
}

function formatSpaceLink(activityItem) {
  const space = findContainer(activityItem, 'SPACE');
  return `<${space.url}|${space.name}>`;
}

export async function formatContentAttachment(activityItem) {    
  const { object } = activityItem;
  const content = await fetchContent(object.localResourceId, ['history', 'metadata.likes', 'space']);
  const title = `${content.title}`;
  const likeCount = content.metadata.likes.count;
  const likes = `${likeCount} ${likeCount === 1 ? 'like' : 'likes'}`;
  return {
      fallback: title,
      color: "#0052CC",
      title: title,
      title_link: object.url,
      footer: `Created by ${content.history.createdBy.displayName} | ${likes} | ${content.space.name}`
  };
}

export async function formatCommentAttachment(activityItem) {  
  const { object } = activityItem;
  const commentId = extractCommentId(object.url);
  const comment = await fetchContent(commentId, ['body.view', 'space']);
  debug(`comment ${JSON.stringify(comment)}`);
  const title = `${comment.title}`;
  return {
      fallback: title,
      color: "#0052CC",
      title: title,      
      title_link: object.url,
      text: html2mrkdwn(comment.body.view.value),
      footer: `${comment.space.name}`
  };
}

function extractCommentId(commentLink) {
  const results = /#comment-(\d+)/.exec(commentLink);
  if (!results) {
    const err = `Couldn't extract comment ID from ${commentLink}`;
    error(err);
    throw new Error(err);
  }
  return results[1];
}

const objectTypes = {
  "BLOGPOST": contentEventFormatters,
  "PAGE": contentEventFormatters
};

export function getEventFormatter(objectType, eventType) {
  const objectFormatters = objectTypes[objectType];
  if (!objectFormatters) {
    return;
  }
  return objectFormatters[eventType];
}