import { fetchIssue } from '../client/jira';
import { findContainer, formatUserLink } from './common';

// By default, the Jira REST API returns all issue fields, which can be 
// an expensive operation for some field configurations. So we constrain the
// query to the fields that we're actually intending to render in our Slack 
// messages.
const DISPLAY_FIELDS = [
  'summary', 'reporter', 'project', 'issuetype', 'status', 'priority', 
  'assignee'
];

const objectTypes = {
  "ISSUE": {
    "CREATED": async (activityItem, actor) => {
      const { object } = activityItem;      
      const issue = await fetchIssue(object.localResourceId, DISPLAY_FIELDS);
      const { issuetype } = issue.fields;
      return {
        text: `*${formatUserLink(activityItem, actor)}* created a new *${issuetype.name}* in *${formatProjectLink(activityItem)}*`,
        attachments: [formatIssueAttachment(issue, object.url)]
      };
    },
    "UPDATED": async (activityItem, actor) => {
      const { object } = activityItem;
      const issue = await fetchIssue(object.localResourceId, DISPLAY_FIELDS);
      const { issuetype } = issue.fields;
      return {
        text: `*${formatUserLink(activityItem, actor)}* updated a *${issuetype.name}* in *${formatProjectLink(activityItem)}*`,
        attachments: [formatIssueAttachment(issue, object.url)]
      };
    },
    "ASSIGNED": async (activityItem, actor) => {
      const { object } = activityItem;
      const issue = await fetchIssue(object.localResourceId, DISPLAY_FIELDS);
      const { issuetype } = issue.fields;
      return {
        text: `*${formatUserLink(activityItem, actor)}* was assigned to a *${issuetype.name}* in *${formatProjectLink(activityItem)}*`,
        attachments: [formatIssueAttachment(issue, object.url)]
      };
    },
    "UNASSIGNED": async (activityItem, actor) => {
      const { object } = activityItem;
      const issue = await fetchIssue(object.localResourceId, DISPLAY_FIELDS);
      const { issuetype } = issue.fields;
      return {
        text: `*${formatUserLink(activityItem, actor)}* was unassigned from a *${issuetype.name}* in *${formatProjectLink(activityItem)}*`,
        attachments: [formatIssueAttachment(issue, object.url)]
      };
    },
    "TRANSITIONED": async (activityItem, actor) => {
      const { object } = activityItem;
      const issue = await fetchIssue(object.localResourceId, DISPLAY_FIELDS);
      const { issuetype, status } = issue.fields;
      return {
        text: `*${formatUserLink(activityItem, actor)}* transitioned a *${issuetype.name}* to *${status.name}* in *${formatProjectLink(activityItem)}*`,
        attachments: [formatIssueAttachment(issue, object.url)]
      };
    },
    "VIEWED": async (activityItem, actor) => {
      const { object } = activityItem;
      const issue = await fetchIssue(object.localResourceId, DISPLAY_FIELDS);
      const { issuetype } = issue.fields;
      return {
        text: `*${formatUserLink(activityItem, actor)}* viewed a *${issuetype.name}* in *${formatProjectLink(activityItem)}*`,
        attachments: [formatIssueAttachment(issue, object.url)]
      };
    }
  }
};

function formatProjectLink(activityItem) {
  const project = findContainer(activityItem, 'PROJECT');
  return `<${project.url}|${project.name}>`;
}

export function formatIssueAttachment(issue, link) {
  const title = `${issue.key}: ${issue.fields.summary}`;
  return {
      fallback: title,
      color: "#0052CC",
      author_name: issue.fields.reporter.displayName,
      author_icon: `${issue.fields.reporter.avatarUrls['48x48']}&format=png`,
      title: title,
      title_link: link,
      fields: [
          {
              title: "Priority",
              value: issue.fields.priority.name,
              short: true
          },
          {
              title: "Status",
              value: issue.fields.status.name,
              short: true
          }
      ],
      thumb_url: `${issue.fields.issuetype.iconUrl.replace('size=xsmall', 'size=xlarge')}&format=png`,
      footer: issue.fields.project.name,
      footer_icon: `${issue.fields.project.avatarUrls['48x48']}&format=png`,
  };
}

export function getEventFormatter(objectType, eventType) {
  const objectFormatters = objectTypes[objectType];
  if (!objectFormatters) {
    return;
  }
  return objectFormatters[eventType];
}
